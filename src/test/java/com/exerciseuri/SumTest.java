package com.exerciseuri;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

class SumTest {

	@Test
	void testGetX() {
		Sum s = new Sum(10,9);
		int expected = 19;
		int actual = s.getX();
		assertTrue(expected == actual );
	}
}
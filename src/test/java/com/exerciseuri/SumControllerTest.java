package com.exerciseuri;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;

@SpringBootTest
@AutoConfigureMockMvc
class SumControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	void testSumCalculator() throws Exception {
		RequestBuilder requestBuilder = get("/sum").param("a","10").param("b", "9").accept(MediaType.APPLICATION_JSON_VALUE);
		
		ResultActions x = mockMvc.perform(requestBuilder );
		
		x.andExpect(status().isOk())
		.andExpect(content().contentType("application/json"))
        .andExpect(jsonPath("$.a").value(10))
        .andExpect(jsonPath("$.b").value(9))
        .andExpect(jsonPath("$.x").value(19));
	}
	//

}

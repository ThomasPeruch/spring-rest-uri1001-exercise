package com.exerciseuri;

public class Sum {
	
	private int a;
	private int b;
	private int x;
	
	public Sum(int a, int b) {
		super();
		this.a = a;
		this.b = b;
		x = a+b;
	}

	public int getA() {
		return a;
	}

	public int getB() {
			return b;
		}

	public int getX( ) {
		return x;
	}

}

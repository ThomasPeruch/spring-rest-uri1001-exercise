package com.exerciseuri;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Uri1002ExerciseApplication {

	public static void main(String[] args) {
		SpringApplication.run(Uri1002ExerciseApplication.class, args);
	}

}

package com.exerciseuri;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;


@RestController
@Api(value="API Rest Sum")
public class SumController {
	
	@GetMapping("/sum")
	public Sum sumCalculator(@RequestParam int a, int b ) {
		Sum s = new Sum( a, b);
		return s;
	}

}
